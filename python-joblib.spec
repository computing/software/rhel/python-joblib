%global srcname joblib

%global common_description						\
Joblib is a set of tools to provide lightweight pipelining in Python.	\
In particular, joblib offers:						\
 * transparent disk-caching of the output values and lazy		\
   re-evaluation (memorize pattern)					\
 * easy simple parallel computing					\
 * logging and tracing of the execution

Name:           python-%{srcname}
Version:        0.11
Release:        4.2%{?dist}
Summary:        Lightweight pipelining: using Python functions as pipeline jobs

License:        BSD
URL:            https://pythonhosted.org/joblib/
Source0:        https://github.com/joblib/joblib/archive/%{version}/%{srcname}-%{version}.tar.gz
# https://github.com/joblib/joblib/issues/691
Patch0:         python-joblib-skiptest.patch

BuildArch:      noarch

%description
%{common_description}

%package -n python%{python3_pkgversion}-%{srcname}
Summary:       %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-numpy
BuildRequires:  python%{python3_pkgversion}-pytest
# Required by doctests
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-sphinx
Requires:       python%{python3_pkgversion}-numpy

%description -n python%{python3_pkgversion}-%{srcname}
%{common_description}

%prep
%autosetup -n %{srcname}-%{version} -p1

%build
%py3_build

%install
%py3_install
#%{__python3} %{py_setup} %{?py_setup_args} build_sphinx
#rm build/sphinx/html/.buildinfo

#%check
#PYTEST_ADDOPTS='-p no:cacheprovider'
#export PYTEST_ADDOPTS
#
#pushd %{buildroot}%{python3_sitelib}
#  py.test-%{python3_version} -p "no:cacheprovider" -v joblib
#popd

%files -n python%{python3_pkgversion}-%{srcname}
# This doc dirctory was excluded because sphinx doc builds are broken: build/sphinx/html
%doc README.rst CHANGES.rst
%license README.rst
%{python3_sitelib}/%{srcname}-*.egg-info
%{python3_sitelib}/%{srcname}/

%changelog
* Sat Oct 5 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.11-4.2
- Drop python3.4, python2 support

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.11-4.1
- Add python3.6 support

* Tue Jun 05 2018 Sergio Pascual <sergiopr@fedoraproject.org> - 0.11-4
- Disable broken test (https://github.com/joblib/joblib/issues/691)
- Disable cache in pytest

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.11-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.11-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Mar 11 2017 Igor Gnatenko <ignatenko@redhat.com> - 0.11-1
- Update to 0.11

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.10.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.10.3-3
- Rebuild for Python 3.6

* Thu Oct 27 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.10.3-2
- New upstream source (0.10.3)
- Add patch to fix a test in python 3.5
- Run all tests

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.0-2
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Wed Jul 13 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.10.0-1
- New upstream source (0.10.0)
- Updated pypi url

* Tue Mar 29 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.9.4-1
- New upstream source (0.9.4)

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Nov 26 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.9.3-3
- Add patch to fix the testing errors

* Tue Nov 24 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.9.3-2
- New upstream release (0.9.3)
- Using new python macros
- Disable failling tests (https://github.com/joblib/joblib/issues/278)

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.4-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Mar 02 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.8.4-1
- New upstream release (0.8.4)

* Wed Sep 03 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.8.3-1
- New upstream release (0.8.3)

* Wed Jul 02 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.8.2-1
- New upstream release (0.8.2)

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Jun 03 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.8.0-2
- Reverted stylistic changes
- Run checks on installed files
- Use tarball from PyPI

* Mon Jun 02 2014 Björn Esser <bjoern.esser@gmail.com> - 0.8.0-1
- new stable upstream
- restructured spec-file
- include README from src-tarball in %%doc
- updated python2-macros
- make testsuite a bit more verbose
- preserve timestamps of modified files
- use tarball from github-tags

* Wed May 14 2014 Bohuslav Kabrda <bkabrda@redhat.com> - 0.8.0-0.2.a2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Thu Jan 09 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.8.0-0.1.a2
- New upstream prerelease (0.8.0a2)

* Sun Aug 25 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.7.1-2
- Removing upstream egg
- Adding BR python(3)-setuptools

* Sat Aug 24 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.7.1-1
- New upstream version (0.7.1)

* Thu Jul 4 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.7.0d-1
- Adding index.rst before importing
